from astropy.io import fits
from astropy.stats import sigma_clipped_stats
from photutils.detection import DAOStarFinder
import numpy as np
from photutils.aperture import CircularAperture
import matplotlib.pyplot as plt
from astropy.visualization import SqrtStretch
from astropy.visualization.mpl_normalize import ImageNormalize
from photutils.segmentation import detect_sources
from astropy.convolution import convolve
from photutils.segmentation import make_2dgaussian_kernel
from photutils.background import Background2D, MedianBackground
from tabulate import tabulate
from photutils.segmentation import deblend_sources
from photutils.segmentation import SourceCatalog
from astropy.visualization import simple_norm

path = 'D:/Fcien/JupCom/TTT/TTT_060_1_alijup_sum.fits'

hdu = fits.open(path)
data = hdu[0].data
hdu.close()


def sources(data,FWHM,thres,plot):
    mean, median, std = sigma_clipped_stats(data, sigma=3.0)
    daofind = DAOStarFinder(fwhm=FWHM, threshold=thres*std)
    sources = daofind(data - median)

    for col in sources.colnames:
        if col not in ('id', 'npix'):
            sources[col].info.format = '%.2f'
    print(sources)

    positions = np.transpose((sources['xcentroid'], sources['ycentroid']))
    apertures = CircularAperture(positions, r=4.0)
    norm = ImageNormalize(stretch=SqrtStretch())
    plt.imshow(data, cmap='Greys', origin='lower', norm=norm,
               interpolation='nearest')
    apertures.plot(color='blue', lw=1.5, alpha=0.5)

    if plot == 'y':
        plt.show()


def segments(data,FWHM,pixels,plot):
    bkg_estimator = MedianBackground()
    bkg = Background2D(data, (50, 50), filter_size=(3, 3),
                       bkg_estimator=bkg_estimator)
    data -= bkg.background  # subtract the background

    threshold = 1.5 * bkg.background_rms

    kernel = make_2dgaussian_kernel(FWHM, size=5)  # FWHM = 3.0
    convolved_data = convolve(data, kernel)

    segment_map = detect_sources(convolved_data, threshold, npixels=pixels)
    print(segment_map)

    norm = ImageNormalize(stretch=SqrtStretch())
    plt.imshow(segment_map, origin='lower', cmap=segment_map.cmap,
               interpolation='nearest')

    if plot == 'y':
        plt.show()

    segm_deblend = deblend_sources(convolved_data, segment_map,
                                   npixels=10, nlevels=32, contrast=0.001,
                                   progress_bar=False)

    cat = SourceCatalog(data, segm_deblend, convolved_data=convolved_data)

    tbl = cat.to_table()

    norm = simple_norm(data, 'sqrt')
    plt.imshow(data, origin='lower', cmap='Greys_r', norm=norm)
    #plt.imshow(segm_deblend, origin='lower', cmap=segm_deblend.cmap,interpolation='nearest')
    cat.plot_kron_apertures(color='yellow',lw=0.5,alpha=0.5)
    #cat.plot_kron_apertures(ax=ax1, color='white', lw=1.5)
    #cat.plot_kron_apertures(ax=ax2, color='white', lw=1.5)

    plt.show()

    with open('table.txt', 'w') as f:
        f.write(tabulate(tbl))


#sources(data,3,8,'y')

segments(data,3,30,'n')







